'''
Created on 1 mar. 2020

@author: eduardo
'''
import glob
import os
import collections
import lucidity

NAME_KEY = 'name'
GROUP_KEY = 'name'
VERSION_KEY = 'version'


def bringReference(template_name, name, version=None, paths=None, recursive=True):
    template_obj = getTemplateByName(template_name, paths=paths, recursive=recursive)
    return bringReferenceTemplate(template_obj, name, version=version)
#     key_dict = {NAME_KEY: name}
#     found_path = getPathToBring(template_name, key_dict, version=version, version_key=VERSION_KEY,
#                                 paths=paths, recursive=recursive)
#     referencePath(found_path, name, template_name)


def bringReferenceTemplate(template_obj, name, version=None):
    key_dict = {NAME_KEY: name}
    found_path = getPathToBring(template_obj, key_dict, version=version, version_key=VERSION_KEY)
    referencePath(found_path, name, template_obj.name)


def referencePath(path, name, ref_type):
    from maya import cmds
    ns = '{}_{}'.format(ref_type, name)
    cmds.file(path, r=1, ns=ns)


def getPathToBring(template_obj, key_dict, version=None, version_key=VERSION_KEY):
#     template_obj = getTemplateByName(template_name, paths=paths, recursive=recursive)
    path_list = globFiles(template_obj, key_dict)

    if version is None:
        return path_list[0]

    grouped_by_version = groupFiles(template_obj, path_list, group_key=version_key)
    version_paths = grouped_by_version.get(version)
    if not version_paths:
        return

    return version_paths[-1]


def getTemplateByName(name, paths=None, recursive=True):
    templates = getTemplates(paths=paths, recursive=recursive)
    if not templates:
        return
    for template in templates:
        if template.name == name:
            return template


def getTemplates(paths=None, recursive=True):
    templates = lucidity.discover_templates(paths=paths, recursive=recursive)
    return templates


def groupFiles(template_obj, path_list, group_key=NAME_KEY):
    groups = collections.defaultdict(list)
    for path in path_list:
        safe_path = path.replace('\\', '/')
        data = template_obj.parse(safe_path)
        key = data.get(group_key)
        groups[key].append(path)
    return groups


def globFiles(template_obj, key_dict):

    # look for all steps for chair
    keys_set = template_obj.keys()
    glob_keys = {k: '*' for k in keys_set}
    glob_keys.update(key_dict)

    glob_path = template_obj.format(glob_keys)

    found_files = glob.iglob(glob_path)
    strict_checked_files = []
    for f in found_files:
        try:
            norm_path = os.path.normpath(f).replace('\\', '/')
            file_data = template_obj.parse(norm_path)

            if not isDataMatch(file_data, key_dict):
                continue

        except Exception, e:  # @UnusedVariable
            continue

        strict_checked_files.append(f.replace('/', '\\'))

    strict_checked_files.sort()
    return strict_checked_files


def isDataMatch(file_data, check_data):
    for k, v in check_data.items():
        if file_data.get(k) != check_data.get(k):
            return False
    return True
