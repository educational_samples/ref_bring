'''
Created on 1 mar. 2020

@author: eduardo
'''
import ref_bring.lucidity as lucidity

PROJECT_PATH = 'D:/work/eclipse_workspace/ref_bring/tests/fake_prj'

# define a template
prop_def = PROJECT_PATH + '/assets/props/{name}/shading/prop_{name}_shading_render_master_v{version}.ma'
prop_template = lucidity.Template('prop', prop_def,
                                  duplicate_placeholder_mode=lucidity.Template.STRICT)
# duplicate_placeholder_mode=lucidity.Template.STRICT -> all equal keys must have equal value

# define a template
set_def = PROJECT_PATH + '/assets/sets/{name}/shading/set_{name}_shading_render_master_v{version}.ma'
set_template = lucidity.Template('set', set_def,
                                 duplicate_placeholder_mode=lucidity.Template.STRICT)
# duplicate_placeholder_mode=lucidity.Template.STRICT -> all equal keys must have equal value


# define another template
anim_def = PROJECT_PATH + '/shots/{sequence}/{name}/anim/shot_{name}_anim_final_master_v{version}.abc'
anim_template = lucidity.Template('anim', anim_def,
                                  duplicate_placeholder_mode=lucidity.Template.STRICT)


def register():
    '''Register templates.'''
    return [
        prop_template,
        set_template,
        anim_template,
    ]

