'''
Created on 1 mar. 2020

# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool folder is

# import and run script
import sys
sys.path.append(path)
sys.path.append(path + r'\ref_bring')  # we need to have lucidity as a module

from ref_bring import ref_bring_interface
reload(ref_bring_interface)
reload(ref_bring_interface.ref_bring_utils)
ui = ref_bring_interface.MainUI(path +r'\ref_bring\templates')
ui.show()


@author: eduardo
'''
from PySide2 import QtWidgets

import loadUiType
import os
import ref_bring_utils


# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'ui/main.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)


class MainUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self, template_path=None):

        # get maya main wndows as a qwidget so we can parent our ui
        parent = getMayaMainWindow()

        # configure parent class and apply ui definition
        super(MainUI, self).__init__(parent=parent)
        self.setupUi(self)

        if template_path is not None:
            self.template_path = template_path

        self.reload_pushButton.clicked.connect(self.onReloadClicked)
        self.bring_breakdown_pushButton.clicked.connect(self.onBringBreakdownClicked)
        self.bring_selected_pushButton.clicked.connect(self.onBringSelectedClicked)
        self.tabWidget.currentChanged.connect(self.onTabChanged)

    @property
    def template_path(self):
        return self.template_path_lineEdit.text()

    @template_path.setter
    def template_path(self, value):
        return self.template_path_lineEdit.setText(value)

    @property
    def breakdown_path(self):
        return self.breakdown_lineEdit.text()

    @breakdown_path.setter
    def breakdown_path(self, value):
        return self.breakdown_lineEdit.setText(value)

    @property
    def skip_existing(self):
        return self.skip_exisitng_checkBox.isChecked()

    @skip_existing.setter
    def skip_existing(self, value):
        return self.skip_exisitng_checkBox.setChecked(value)

    def onReloadClicked(self):
        self.reload()

    def onBringBreakdownClicked(self):
        QtWidgets.QMessageBox.information(self, 'information!', 'Not implemented yet!')

    def onBringSelectedClicked(self):
        widget = self.tabWidget.currentWidget()
        template = widget.my_template
        items = widget.my_list.selectedItems()
        item_names = [i.text() for i in items]
        for name in item_names:
            ref_bring_utils.bringReferenceTemplate(template, name, version=None)

    def onTabChanged(self):
        print 'tabChanged'
        self.updateCurrentTab()

    def reload(self):
        self.populateTabs()
        self.updateCurrentTab()

    def populateTabs(self):
        # clear tabs
        for i in range(self.tabWidget.count() - 1):
            self.tabWidget.removeTab(0)

        template_path = self.template_path
        template_list = ref_bring_utils.getTemplates([template_path], recursive=False)
        for template in template_list:
            self._addTab(template, template_path)

    def _addTab(self, template, template_path):
        name = template.name

        widget = QtWidgets.QWidget()
        widget.my_template = template
        widget.my_path = template_path

        layout = QtWidgets.QVBoxLayout()
        widget.setLayout(layout)
        list_widget = QtWidgets.QListWidget()
        list_widget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        widget.my_list = list_widget

        layout.addWidget(list_widget)

#         self.tabWidget.addTab(widget, name)
        self.tabWidget.insertTab(0, widget, name)

    def updateCurrentTab(self):
#         current_index = self.tabWidget.currentIndex()
        widget = self.tabWidget.currentWidget()
        if not hasattr(widget, 'my_template'):
            return
        widget.my_list.clear()
        template_obj = widget.my_template
        path_list = ref_bring_utils.globFiles(template_obj, key_dict={})
        grouped_by_version = ref_bring_utils.groupFiles(template_obj, path_list,
                                                        group_key=ref_bring_utils.NAME_KEY)
        names = sorted(grouped_by_version.keys())
        for name in names:
            item = QtWidgets.QListWidgetItem(name)
            tooltip = grouped_by_version[name][-1]
            item.setToolTip(tooltip)
            widget.my_list.addItem(item)


def getMayaMainWindow():
    ''' Tries to get the maya main window
    Args:
        None
    Returns:
        QWidget or None: the maya main window or None if it could not be found
    '''

    try:
        # we need to wrap a pointer as a pyside widget so we need help!
        from shiboken2 import wrapInstance
        from maya import OpenMayaUI as omui

        # get opinter object
        ptr = omui.MQtUtil.mainWindow()

        # cast pointer to widget (may fail)
        main_window = wrapInstance(long(ptr), QtWidgets.QWidget)

    except Exception, e:  # @UnusedVariable
        main_window = None

    return main_window
